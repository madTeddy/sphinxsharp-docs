.. default-domain:: sphinxsharp

Overview
========

**CSharp** (C#) domain for `Sphinx <https://github.com/sphinx-doc/sphinx>`_.

Installing
----------

Install the extension using **pip**:

.. code-block:: python

    pip install sphinxsharp

Or install extension with **custom styling**:

.. code-block:: python

    pip install sphinxsharp-pro

.. note:: This documentation using a `sphinxsharp-pro <https://github.com/madTeddy/sphinxsharp-pro>`_ extension.

Usage
-----

To enable the extension, you need to add the following to your **conf.py**:

.. code-block:: python

    extensions = ['sphinxsharp.sphinxsharp'] # for sphinxsharp with default styling
    extensions = ['sphinxsharp-pro.sphinxsharp'] # for sphinxsharp with custom styling

.. note:: With custom styling you can define your styles (colors etc.) for syntax highlightning using **css**.

In your **.rst documents** you can use it with explicit directive declaration:

.. code-block:: rst

    .. sphinxsharp:type:: public class Example

Or for shorter version, define this domain as **default**:

.. code-block:: rst

    .. default-domain:: sphinxsharp

    .. type:: public class Example

.. seealso:: See also list of available :ref:`directives`.

.. toctree::
    :maxdepth: 2

    self
    styling
    directives

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`